package br.com.planb.entity;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("funcionario")
public class Funcionario {

	@PrimaryKey("id")
	private long id;
	
	@Column("name")
	private String name;
	
	@Column
	private int age;
	
	@Column(value="salary")
	private float salary;

	public Funcionario(long id, String name, int age, float salary) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public Funcionario() {
		super();
	}

	@Override
	public String toString() {
		return "funcionario [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}
	
	
}
