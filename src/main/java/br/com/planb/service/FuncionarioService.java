package br.com.planb.service;

import java.util.List;

import br.com.planb.entity.Funcionario;

public interface FuncionarioService {
	
	public void createFuncionario(Funcionario funcionario);
    
	public Funcionario getFuncionario(int id);

    public void updateFuncionario(Funcionario funcionario);
    
    public void deleteFuncionario(int id);
    
    public List<Funcionario> getAllFuncionarios();
}
