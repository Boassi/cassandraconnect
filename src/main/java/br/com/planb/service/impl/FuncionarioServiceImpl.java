package br.com.planb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.planb.dao.FuncionarioDAO;
import br.com.planb.dao.impl.FuncionarioDAOImpl;
import br.com.planb.entity.Funcionario;
import br.com.planb.service.FuncionarioService;

@Service
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired  
    private FuncionarioDAOImpl funcionarioDAO;

	
	 public FuncionarioServiceImpl() {
	        super();    
	   }
	
	public void createFuncionario(Funcionario funcionario) {
		funcionarioDAO.createFuncionario(funcionario);
	}

	public Funcionario getFuncionario(int id) {
		return funcionarioDAO.getFuncionario(id);
	}

	public void updateFuncionario(Funcionario funcionario) {
		funcionarioDAO.updateFuncionario(funcionario);
	}

	public void deleteFuncionario(int id) {
		funcionarioDAO.deleteFuncionario(id);
		
	}

	public List<Funcionario> getAllFuncionarios() {
		return funcionarioDAO.getAllFuncionario();
	}

}
