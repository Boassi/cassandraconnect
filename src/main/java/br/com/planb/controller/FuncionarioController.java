package br.com.planb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.planb.entity.Funcionario;
import br.com.planb.service.FuncionarioService;
import br.com.planb.service.impl.FuncionarioServiceImpl;

@RestController
public class FuncionarioController {

	@Autowired
	FuncionarioServiceImpl funcionarioService;
	
	public FuncionarioController()
	{
		System.out.println("FuncionarioController()");
	}
	
	@RequestMapping(value="/funcionario",method=RequestMethod.POST)
	String create(@RequestBody Funcionario funcionario)
	{
		funcionarioService.createFuncionario(funcionario);
		return "OK";
		
	}
	
	@RequestMapping(value="/funcionario/{id}",method=RequestMethod.DELETE)
	void delete(@PathVariable("id") int id)
	{
		funcionarioService.deleteFuncionario(id);
	}
	
	@RequestMapping(value="/funcionario",method=RequestMethod.GET)
	List<Funcionario> lista()
	{
		return funcionarioService.getAllFuncionarios();
	}
	
	@RequestMapping(value="/funcionario/{id}",method=RequestMethod.GET)
	Funcionario busca(@PathVariable("id") int id)
	{
		return funcionarioService.getFuncionario(id);
	}
	
	@RequestMapping(value="/funcionario",method=RequestMethod.PUT)
	String atualiza(@RequestBody Funcionario funcionario)
	{
		funcionarioService.updateFuncionario(funcionario);
		return "ok";
	}
}
