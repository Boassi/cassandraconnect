package br.com.planb.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;
import org.springframework.data.cassandra.core.StatementFactory;
import org.springframework.data.cassandra.core.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.convert.UpdateMapper;
import org.springframework.data.cassandra.core.cql.WriteOptions;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;

import br.com.planb.entity.Funcionario;

@Repository
public class MyCassandraTemplate {
	
    @Autowired
    private CassandraOperations cassandraTemplate;
    
    /** MyCassandraTemplate Default constructor*/
    public MyCassandraTemplate() {
        System.out.println("MyCassandraTemplate()");
    }
    
    public <T> void create(T entity) {
    	cassandraTemplate.insert(entity, InsertOptions.builder().ifNotExists(true).build());
    }
    public <T> void update(T entity) {     
        cassandraTemplate.update(entity);
    }
    public <T> T findById(Object id, Class<T> claz) {
        return cassandraTemplate.selectOneById(id,claz);
    }
    public <T> void deleteById(Object id, Class<T> claz) {
        cassandraTemplate.deleteById(id,claz);
        
    }	
    public <T> List<T> findAll(Class<T> claz) {    	
        return (List<T>) cassandraTemplate.select("SELECT * FROM funcionario", claz);
    }
   
}
