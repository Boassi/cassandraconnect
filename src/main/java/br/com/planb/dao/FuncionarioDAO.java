package br.com.planb.dao;

import java.util.List;

import br.com.planb.entity.Funcionario;

public interface FuncionarioDAO {
	
	 public void createFuncionario(Funcionario funcionario);
		
	 public Funcionario getFuncionario(int id);
	
	 public void updateFuncionario(Funcionario funcionario);
	 
	 public void deleteFuncionario(int id);
	 
	 public List<Funcionario> getAllFuncionario();
}
