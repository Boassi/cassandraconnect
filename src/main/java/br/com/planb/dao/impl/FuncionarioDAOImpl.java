package br.com.planb.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.planb.dao.FuncionarioDAO;
import br.com.planb.entity.Funcionario;
import br.com.planb.util.MyCassandraTemplate;

@Repository
public class FuncionarioDAOImpl implements FuncionarioDAO{
	
	 @Autowired
	    private MyCassandraTemplate myCassandraTemplate;

	public void createFuncionario(Funcionario funcionario) {
		myCassandraTemplate.create(funcionario);
	}

	public Funcionario getFuncionario(int id) {
		return myCassandraTemplate.findById(id, Funcionario.class);
	}
	
	public void updateFuncionario(Funcionario funcionario) {
		myCassandraTemplate.update(funcionario);
	}

	public void deleteFuncionario(int id) {
	  myCassandraTemplate.deleteById(id,Funcionario.class);
		
	}

	public List<Funcionario> getAllFuncionario() {
		return myCassandraTemplate.findAll(Funcionario.class);
	}

}
